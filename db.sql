-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET time_zone = "+00:00";

-- -- Create Database if not hosted on azure

-- CREATE DATABASE IF NOT EXISTS `container-db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
-- USE `container-db`;

-- -- --------------------------------------------------------

-- -- Table structure for table `tbl_people`
-- DROP TABLE IF EXISTS `tbl_users`;
-- CREATE TABLE `tbl_users` (
--   `ID` int(11) NOT NULL AUTO_INCREMENT,
--   `EMAIL` varchar(30) NOT NULL,
--   `PASSWORD` varchar(50) NOT NULL,
--   PRIMARY KEY ( `ID` )
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -- Clear data for table `tbl_users`
-- -- TRUNCATE TABLE `tbl_users`
-- INSERT INTO `tbl_users` (`EMAIL`, `PASSWORD`) VALUES ('Admin', 'admin123');
-- INSERT INTO `tbl_users` (`EMAIL`, `PASSWORD`) VALUES ('Aman', 'oye123');
-- INSERT INTO `tbl_users` (`EMAIL`, `PASSWORD`) VALUES ('Arun', 'tuu321');
-- INSERT INTO `tbl_users` (`EMAIL`, `PASSWORD`) VALUES ('Raman', 'shad258');
-- INSERT INTO `tbl_users` (`EMAIL`, `PASSWORD`) VALUES ('Avan', 'Avan123');
-- INSERT INTO `tbl_users` (`EMAIL`, `PASSWORD`) VALUES ('Alin', 'Alin123');
-- INSERT INTO `tbl_users` (`EMAIL`, `PASSWORD`) VALUES ('Ram', 'Ram123');