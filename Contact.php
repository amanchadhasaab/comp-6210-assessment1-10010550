<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accounting</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Footer-Clean.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/Testimonials.css">
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-md">
        <div class="container-fluid"><a class="navbar-brand" href="#" style="background-image:url(&quot;assets/img/GeyserlandSBA-flattened.png&quot;);width:190px;height:120px;background-color:#eaeaea;background-repeat:no-repeat;background-size:120%;background-position:top;color:rgba(7,7,7,0.9);"></a>
            <button
                class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto" style="background-color:#bbb8b8;">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="Home.php" target="_blank" style="font-size:16px;"><strong>Home</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="Contact.php"><strong>Contact</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="About.php"><strong>About</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="Services.php"><strong>Services</strong></a></li>
                        <l1><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#SignUp">
                                Sign Up
                            </button>
                           
                    </ul>
                </div>
        </div>
    </nav>
    <h1 class="text-center" style="background-color:rgb(136,127,127);">Contact Us</h1>
    <h3 class="text-left">Join by filling this form:-</h3>
    <div class="register-photo">
        <div class="form-container">
            <div class="image-holder"></div>
            <form method="post">
                <h2 class="text-center"><strong>Create</strong> an account.</h2>
                <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email"></div>
                <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password"></div>
                <div class="form-group"><input class="form-control" type="password" name="password-repeat" placeholder="Password (repeat)"></div>
                <div class="form-group">
                    <div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox">I agree to the license terms.</label></div>
                </div>
                <div class="form-group"><button class="btn btn-primary btn-block" type="submit">Sign Up</button></div><a href="#" class="already">You already have an account? Login here.</a></form>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="background-color:#474444;">
                    <form method="post">
                        <h2 class="text-center">Sign In</h2>
                        <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email"></div>
                        <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password"></div>
                        <div class="form-group">
                            <div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox">I agree to the license terms.</label></div>
                        </div>
                        <div class="form-group"><button class="btn btn-primary btn-block" type="submit">Sign In</button></div><a href="#" class="already">You already have an account? Login here.</a></form>
                </div>
                <div class="col-md-6"><iframe allowfullscreen="" frameborder="0" width="100%" height="400" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAQmwJfOz1g0qnz7kS9g0ikaDZP9q37Njs&amp;q=tauranga%2C+new+zealand&amp;zoom=11"></iframe></div>
            </div>
        </div>
    </div>
    <div class="footer-clean">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm item policy">
                        <button type="button" class="btn btn-link " data-toggle="modal" data-target="#Policy">Policy Document</button>
                    </div>
                    <div class="col-sm item contact">
                        <a href="Contact.php" style="font-size:40px;color:rgb(0,0,0);">Contact</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="modal fade" id="SignUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <h1>Sign Up</h1>
                            <p>Please fill in this form to create an account.</p>
                            <hr>
                            <label for="email">
                                <b>Email</b>
                            </label>
                            <br>
                            <input type="text" placeholder="Enter Email" name="email" required>
                            <br>
    
                            <label for="psw">
                                <b>Password</b>
                            </label>
                            <br>
                            <input type="password" placeholder="Enter Password" name="psw" required>
                            <br>
    
                            <label for="psw-repeat">
                                <b>Repeat Password</b>
                            </label>
                            <br>
                            <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
    
                            <label>
                                <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
                            </label>
    
                            <p>By creating an account you agree to our
                                <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="Policy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentesque, felis vel varius bibendum, orci nibh laoreet nisi, sollicitudin maximus massa dolor at orci. Vestibulum mi magna, posuere vel augue eu, fringilla varius lorem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas facilisis, nisi vel auctor sodales, est mi facilisis libero, et luctus quam ligula non turpis. Ut euismod enim vel tortor ornare, vitae sollicitudin tortor consequat. Nunc non nisl sapien. Vivamus ut mollis ligula. Vivamus porta placerat orci, et fringilla eros accumsan non. Praesent euismod tristique accumsan. Aenean nec venenatis lacus. Proin sed ultricies nunc, quis porttitor ante.
                        
                        Donec varius efficitur ligula ac malesuada. Duis euismod, urna nec pellentesque imperdiet, tellus quam consequat urna, eu ultrices mi tortor ut nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent enim nunc, vehicula a egestas eu, pharetra at enim. Vestibulum suscipit quam nisl, vel pretium lacus consequat eget. Vivamus maximus ornare dignissim. Proin et dui justo. Ut lobortis risus semper porttitor blandit.
                        
                        In ultricies, orci a hendrerit finibus, nulla nibh vehicula orci, a sodales tellus magna et lectus. Nullam at turpis at arcu posuere sagittis. Suspendisse lacus erat, pharetra a varius quis, pharetra nec neque. Fusce quis sem nec sem semper ultrices. Sed neque sem, vestibulum ut iaculis sed, malesuada et est. Fusce ipsum arcu, molestie tincidunt neque non, facilisis sollicitudin diam. Nullam condimentum urna libero, iaculis gravida magna scelerisque vel. Sed a arcu non augue elementum ullamcorper. Integer fringilla dui eget est porta, ac venenatis lacus commodo. Vivamus diam sapien, sollicitudin ac volutpat nec, pharetra nec purus.
                        
                        Vestibulum nunc mauris, aliquet vitae lectus eu, ultrices vulputate ex. Proin eleifend risus et tincidunt rutrum. Donec in augue non ex sagittis auctor. Morbi tortor odio, ultrices vel eros sit amet, rutrum dapibus nisl. Curabitur aliquet magna vel nunc ultricies, a faucibus tellus pharetra. In ut fermentum magna. Proin ultrices lacinia vulputate. Donec sed neque eget nibh pharetra gravida. Donec libero est, sollicitudin nec ultricies et, imperdiet tincidunt est. Cras nec ex congue, condimentum lacus finibus, viverra risus. Nullam sit amet imperdiet ligula. Cras viverra massa metus, non condimentum nunc congue in. Nullam dapibus dui ligula, sit amet ultricies turpis porttitor ac. Aenean quis augue eget nulla porttitor egestas vel in dui.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>