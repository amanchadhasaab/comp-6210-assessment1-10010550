<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accounting</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Footer-Clean.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/Testimonials.css">
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-md">
        <div class="container-fluid"><a class="navbar-brand" href="#" style="background-image:url(&quot;assets/img/GeyserlandSBA-flattened.png&quot;);width:190px;height:120px;background-color:#eaeaea;background-repeat:no-repeat;background-size:120%;background-position:top;color:rgba(7,7,7,0.9);"></a>
            <button
                class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto" style="background-color:#bbb8b8;">
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="Home.php" target="_blank" style="font-size:16px;"><strong>Home</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="Contact.php"><strong>Contact</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="About.php"><strong>About</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="Services.php"><strong>Services</strong></a></li>
                        <l1><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#SignUp">
                                Sign Up
                            </button>
                            
                            
                    </ul>
                </div>
        </div>
    </nav>
    <div class="row">
        <div class="col"><img src="assets/img/coloured-accounting-background-design_1151-88.jpg" width="100%" height="200px" style="width:444px;height:318px;margin:10px;"></div>
        <div class="col">
            <h2 class="text-center">Who are we</h2>
            <p>Phasellus pharetra pellentesque sapien eu dictum. Donec dictum facilisis sapien, et pellentesque elit aliquam at. Morbi in lobortis lacus. Nulla luctus est a quam ultricies, vitae lobortis arcu rhoncus. Etiam ex tortor, iaculis eu ex a, efficitur venenatis justo. Interdum et malesuada fames ac ante ipsum primis in faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus felis ipsum, dignissim ac egestas eget, ultricies id ex. Aliquam vestibulum mauris at augue congue ultricies. Quisque venenatis felis nec ligula commodo, eu lobortis erat facilisis. Sed tempor id lorem in sagittis.<br><br></p>
        </div>
    </div>
    <div class="testimonials-clean">
        <div class="container">
            <div class="intro">
                <div class="row">
                    <div class="col">
                        <h3 class="text-center"><br><strong>Testimonials</strong><br><br></h3>
                    </div>
                </div>
            </div>
            <div class="row people">
                <div class="col-md-6 col-lg-4 item">
                    <div class="box">
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.</p>
                    </div>
                    <div class="author">
                        <h5 class="name" style="color:rgb(255,255,255);">Ashu Ghotra</h5>
                        <p class="title">CEO of Company Inc.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <div class="box">
                        <p class="description">Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id.</p>
                    </div>
                    <div class="author">
                        <h5 class="name" style="color:rgb(255,255,255);">Salman Khan</h5>
                        <p class="title">Founder of Style Co.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <div class="box">
                        <p class="description">Aliquam varius finibus est, et interdum justo suscipit. Vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu.</p>
                    </div>
                    <div class="author">
                        <h5 class="name" style="color:rgb(251,251,251);">Brock</h5>
                        <p class="title">Owner of Creative Ltd.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm"><iframe width="100%" height="100%" allowfullscreen="" frameborder="0" src="https://www.youtube.com/embed/UPqpy8dsEnQ?controls=0" style="color:rgb(247,247,247);"></iframe></div>
                <div class="col-sm"><a class="weatherwidget-io" href="https://forecast7.com/en/n37d69176d17/tauranga/" data-label_1="TAURANGA" data-label_2="WEATHER" data-theme="original">TAURANGA WEATHER</a>
        <script>
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
        </script></div>
            </div>
        </div>
    </div>
    <div class="footer-clean">
        <footer>
            <div class="container">
                <div class="row justify-content-center">
                        <button type="button" class="btn btn-link " data-toggle="modal" data-target="#Policy">
                                Policy
                            </button>
                    <div class="col-sm-4 col-md-3 item">
                        <ul>
                            <li></li>
                        </ul><a href="Contact.php" style="font-size:40px;color:rgb(0,0,0);">Contact</a></div>
                </div>
            </div>
        </footer>
    </div>
    <div class="modal fade" id="SignUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <h1>Sign Up</h1>
                            <p>Please fill in this form to create an account.</p>
                            <hr>
                            <label for="email">
                                <b>Email</b>
                            </label>
                            <br>
                            <input type="text" placeholder="Enter Email" name="email" required>
                            <br>
    
                            <label for="psw">
                                <b>Password</b>
                            </label>
                            <br>
                            <input type="password" placeholder="Enter Password" name="psw" required>
                            <br>
    
                            <label for="psw-repeat">
                                <b>Repeat Password</b>
                            </label>
                            <br>
                            <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
    
                            <label>
                                <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
                            </label>
    
                            <p>By creating an account you agree to our
                                <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="Policy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentesque, felis vel varius bibendum, orci nibh laoreet nisi, sollicitudin maximus massa dolor at orci. Vestibulum mi magna, posuere vel augue eu, fringilla varius lorem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas facilisis, nisi vel auctor sodales, est mi facilisis libero, et luctus quam ligula non turpis. Ut euismod enim vel tortor ornare, vitae sollicitudin tortor consequat. Nunc non nisl sapien. Vivamus ut mollis ligula. Vivamus porta placerat orci, et fringilla eros accumsan non. Praesent euismod tristique accumsan. Aenean nec venenatis lacus. Proin sed ultricies nunc, quis porttitor ante.
                        
                        Donec varius efficitur ligula ac malesuada. Duis euismod, urna nec pellentesque imperdiet, tellus quam consequat urna, eu ultrices mi tortor ut nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent enim nunc, vehicula a egestas eu, pharetra at enim. Vestibulum suscipit quam nisl, vel pretium lacus consequat eget. Vivamus maximus ornare dignissim. Proin et dui justo. Ut lobortis risus semper porttitor blandit.
                        
                        In ultricies, orci a hendrerit finibus, nulla nibh vehicula orci, a sodales tellus magna et lectus. Nullam at turpis at arcu posuere sagittis. Suspendisse lacus erat, pharetra a varius quis, pharetra nec neque. Fusce quis sem nec sem semper ultrices. Sed neque sem, vestibulum ut iaculis sed, malesuada et est. Fusce ipsum arcu, molestie tincidunt neque non, facilisis sollicitudin diam. Nullam condimentum urna libero, iaculis gravida magna scelerisque vel. Sed a arcu non augue elementum ullamcorper. Integer fringilla dui eget est porta, ac venenatis lacus commodo. Vivamus diam sapien, sollicitudin ac volutpat nec, pharetra nec purus.
                        
                        Vestibulum nunc mauris, aliquet vitae lectus eu, ultrices vulputate ex. Proin eleifend risus et tincidunt rutrum. Donec in augue non ex sagittis auctor. Morbi tortor odio, ultrices vel eros sit amet, rutrum dapibus nisl. Curabitur aliquet magna vel nunc ultricies, a faucibus tellus pharetra. In ut fermentum magna. Proin ultrices lacinia vulputate. Donec sed neque eget nibh pharetra gravida. Donec libero est, sollicitudin nec ultricies et, imperdiet tincidunt est. Cras nec ex congue, condimentum lacus finibus, viverra risus. Nullam sit amet imperdiet ligula. Cras viverra massa metus, non condimentum nunc congue in. Nullam dapibus dui ligula, sit amet ultricies turpis porttitor ac. Aenean quis augue eget nulla porttitor egestas vel in dui.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>