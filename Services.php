<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accounting</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Footer-Clean.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/Testimonials.css">
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-md">
        <div class="container-fluid"><a class="navbar-brand" href="#" style="background-image:url(&quot;assets/img/GeyserlandSBA-flattened.png&quot;);width:190px;height:120px;background-color:#eaeaea;background-repeat:no-repeat;background-size:120%;background-position:top;color:rgba(7,7,7,0.9);"></a>
            <button
                class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto" style="background-color:#bbb8b8;">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="Home.php" target="_blank" style="font-size:16px;"><strong>Home</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="Contact.php"><strong>Contact</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="About.php"><strong>About</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="Services.php"><strong>Services</strong></a></li>
                        <l1><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#SignUp">
                                Sign Up
                            </button>
                            
                           
                    </ul>
                </div>
        </div>
    </nav>
    <h1 class="text-center">Services:-</h1>
    <div>
        <div class="container">
            <div class="row services">
                <div class="col-md-4">
                    <h3>Budgeting</h3>
                    <p>Massa in vehicula pulvinar eu quia, ligula urna augue in purus sociis tellus, sociis odio tincidunt donec, sed hac. Erat auctor dolor eget posuere quis eu, at feugiat. Ac consectetuer id et felis. Lobortis a nascetur, mi fringilla. Ligula pellentesque id tempus in sodales, eget et posuere pretium arcu etiam malesuada, at nam urna nulla at est qui. In dapibus. Habitant amet a, suspendisse gravida, ut malesuada donec varius non at dui. Sed iaculis suscipit dapibus mauris lectus odio, fermentum eleifend fusce, lorem non pellentesque ut, lacus donec est habitant, mauris conubia nulla dignissim lobortis omnis. Neque nunc vestibulum, ullamcorper a accumsan nostra, laborum id. Ultrices magna nihil tempor odio et, bibendum semper ante vel tristique mauris a. Donec amet suspendisse. Vestibulum porttitor tincidunt, aliquam elit, dui semper velit dapibus, nisl ac urna ac.
                        
                        </p>
                </div>
                <div class="col-md-4">
                    <h3>payroll</h3>
                    <p>Proin convallis ultricies nibh. Morbi et ipsum at est laoreet rutrum. Fusce vitae fermentum neque, vel varius eros. Integer viverra euismod finibus. Nunc tincidunt auctor interdum. Suspendisse ut nulla a lacus finibus aliquam ac a velit. Aliquam at augue gravida, pretium massa sit amet, rhoncus nunc. Aenean commodo dolor augue, eget gravida dui tristique sit amet. Vestibulum augue eros, sagittis sit amet arcu eu, tristique euismod orci. Aenean luctus porttitor libero, scelerisque fringilla enim sagittis eget. Curabitur ac pellentesque libero, et cursus ligula. Nam nec justo vitae lectus tempor lacinia. Nulla pretium ipsum nunc, at vulputate erat laoreet vitae. Nam non elit interdum, ullamcorper tellus id, elementum felis.</p>
                </div>
                <div class="col-md-4">
                    <h3>Auto-enrolment</h3>
                    <p>Curabitur pulvinar nulla eu rutrum ullamcorper. Mauris fermentum nec nibh quis sodales. Integer semper libero sit amet bibendum scelerisque. Mauris iaculis neque dui, a porta augue varius quis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse tortor lectus, commodo vel consectetur eget, hendrerit quis lacus. Proin magna sapien, commodo ut massa non, scelerisque aliquam lacus. Nullam dapibus odio nec scelerisque pulvinar.</p>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container" style="background-color:#2a2a2a;">
            <h2 class="text-center" style="padding:0px;background-color:#171717;color:rgb(255,255,255);">Feedback and Questions</h2>
            <div class="row">
                <div class="col-md-6" style="color:rgb(255,255,255);">
                    <h3>FAQs:-</h3>
                    <p><br><strong>Frequently asked questions</strong>&nbsp;(<strong>FAQ</strong>) or Questions and Answers (Q&amp;A), are listed questions and answers, all supposed to be commonly asked in some context, and pertaining to a particular topic.
                        The format is commonly used on email mailing lists and other online forums, where certain common questions tend to recur.<br><br></p>
                </div>
                <div class="col-md-6" style="color:rgb(255,255,255);">
                    <h3>How easy it is to change accountants?</h3>
                    <p>Praesent id urna ac dolor pharetra commodo. Vestibulum blandit sem vel velit tincidunt faucibus. Pellentesque finibus non erat vitae convallis. Aenean leo justo, rutrum eu venenatis nec, rhoncus sed metus. Mauris a mattis nisl, non cursus massa. Nam lobortis nisl consequat, dictum tellus at, posuere sapien. Nulla malesuada mi vitae lobortis blandit. Curabitur mollis imperdiet ipsum, a viverra neque eleifend non. Morbi in eros lacinia, mattis eros gravida, maximus lorem. Ut a magna sed leo luctus consectetur id sit amet leo. Aliquam rutrum gravida gravida. Aenean vel volutpat augue.</p>
                    <h3>What information is needed to receive a quote and advice?</h3>
                    <p>Morbi magna odio, tincidunt sed luctus vitae, interdum sit amet massa. Nulla sit amet ornare orci. Vivamus finibus felis elementum diam viverra, nec mattis neque venenatis. Integer euismod sapien nec lacus feugiat hendrerit. Sed id consequat leo. Proin imperdiet justo nulla, sed ultricies est aliquam sed. Sed et elementum eros. Vivamus dignissim suscipit orci ut placerat. Vivamus et pulvinar est, in tristique quam. Mauris justo neque, laoreet in consectetur ac, blandit a velit. Ut viverra tristique ante non pharetra. Sed sed tempor nunc. Sed vehicula blandit lorem et dapibus.</p>
                    <h3>How long has the practice been operating?</h3>
                    <p>Mauris at arcu id ex interdum eleifend. Maecenas a posuere mi, sed interdum velit. Donec commodo massa sed metus euismod faucibus in in elit. Vestibulum auctor nisi sit amet velit lobortis, sed rutrum urna lacinia. Nunc accumsan accumsan massa, vitae sagittis risus euismod ac. Maecenas faucibus luctus sem a viverra. In ac aliquam velit, a pellentesque nunc. Donec cursus ac lorem ut hendrerit. Cras quis augue id magna fringilla rhoncus id vitae enim. Vivamus sed sapien leo. Sed vitae nibh pretium, consectetur ex vel, fermentum est. Suspendisse nec aliquet leo, vel convallis nisl. Nulla vulputate, metus in congue volutpat, arcu dolor iaculis nulla, eu feugiat justo dolor quis nibh. Nullam convallis mauris ac malesuada malesuada. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent molestie bibendum rhoncus.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-clean">
        <footer>
            <div class="container">
                <div class="row justify-content-center">
                        <button type="button" class="btn btn-link " data-toggle="modal" data-target="#Policy">
                                Policy
                            </button>
                    <div class="col-sm-4 col-md-3 item">
                        <ul>
                            <li></li>
                        </ul><a href="Contact.php" style="font-size:40px;color:rgb(0,0,0);">Contact</a></div>
                </div>
            </div>
        </footer>
    </div>
    <div class="modal fade" id="SignUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <h1>Sign Up</h1>
                            <p>Please fill in this form to create an account.</p>
                            <hr>
                            <label for="email">
                                <b>Email</b>
                            </label>
                            <br>
                            <input type="text" placeholder="Enter Email" name="email" required>
                            <br>
    
                            <label for="psw">
                                <b>Password</b>
                            </label>
                            <br>
                            <input type="password" placeholder="Enter Password" name="psw" required>
                            <br>
    
                            <label for="psw-repeat">
                                <b>Repeat Password</b>
                            </label>
                            <br>
                            <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
    
                            <label>
                                <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
                            </label>
    
                            <p>By creating an account you agree to our
                                <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="Policy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentesque, felis vel varius bibendum, orci nibh laoreet nisi, sollicitudin maximus massa dolor at orci. Vestibulum mi magna, posuere vel augue eu, fringilla varius lorem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas facilisis, nisi vel auctor sodales, est mi facilisis libero, et luctus quam ligula non turpis. Ut euismod enim vel tortor ornare, vitae sollicitudin tortor consequat. Nunc non nisl sapien. Vivamus ut mollis ligula. Vivamus porta placerat orci, et fringilla eros accumsan non. Praesent euismod tristique accumsan. Aenean nec venenatis lacus. Proin sed ultricies nunc, quis porttitor ante.
                        
                        Donec varius efficitur ligula ac malesuada. Duis euismod, urna nec pellentesque imperdiet, tellus quam consequat urna, eu ultrices mi tortor ut nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent enim nunc, vehicula a egestas eu, pharetra at enim. Vestibulum suscipit quam nisl, vel pretium lacus consequat eget. Vivamus maximus ornare dignissim. Proin et dui justo. Ut lobortis risus semper porttitor blandit.
                        
                        In ultricies, orci a hendrerit finibus, nulla nibh vehicula orci, a sodales tellus magna et lectus. Nullam at turpis at arcu posuere sagittis. Suspendisse lacus erat, pharetra a varius quis, pharetra nec neque. Fusce quis sem nec sem semper ultrices. Sed neque sem, vestibulum ut iaculis sed, malesuada et est. Fusce ipsum arcu, molestie tincidunt neque non, facilisis sollicitudin diam. Nullam condimentum urna libero, iaculis gravida magna scelerisque vel. Sed a arcu non augue elementum ullamcorper. Integer fringilla dui eget est porta, ac venenatis lacus commodo. Vivamus diam sapien, sollicitudin ac volutpat nec, pharetra nec purus.
                        
                        Vestibulum nunc mauris, aliquet vitae lectus eu, ultrices vulputate ex. Proin eleifend risus et tincidunt rutrum. Donec in augue non ex sagittis auctor. Morbi tortor odio, ultrices vel eros sit amet, rutrum dapibus nisl. Curabitur aliquet magna vel nunc ultricies, a faucibus tellus pharetra. In ut fermentum magna. Proin ultrices lacinia vulputate. Donec sed neque eget nibh pharetra gravida. Donec libero est, sollicitudin nec ultricies et, imperdiet tincidunt est. Cras nec ex congue, condimentum lacus finibus, viverra risus. Nullam sit amet imperdiet ligula. Cras viverra massa metus, non condimentum nunc congue in. Nullam dapibus dui ligula, sit amet ultricies turpis porttitor ac. Aenean quis augue eget nulla porttitor egestas vel in dui.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>